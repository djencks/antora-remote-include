'use strict'

//from asciidoc-loader
const Opal = global.Opal

//from content-aggregator
const { createHash } = require('crypto')
const fs = require('fs-extra')
const getCacheDir = require('cache-directory')
const expandPath = require('@antora/expand-path-helper')
const ospath = require('path')
// QUESTION should the cache version track the major version of Antora?
const CONTENT_CACHE_FOLDER = 'content/2'
const REMOTE_INCLUDES_FOLDER = 'remoteIncludes'

//native
const http = require('http')
const https = require('https')

//events
const AFTER_CLASSIFY_CONTENT = 'afterClassifyContent'
const BEFORE_CONVERT_DOCUMENTS = 'beforeConvertDocuments'

const REMOTE_INCLUDE_RX_TEST = /^include::(http|file)/gm
const REMOTE_INCLUDE_RX = /^include::(https?|file):\/\/(.*)\[/gm

module.exports.register = (eventEmitter, config = {}) => {
  const allowHttp = config.allowHttp
  const tlsRejectUnauthorized = config.TESTINGRejectUnauthorized !== 'test.only.false'
  const allowedFilePaths = config.allowedFilePaths || []

  const remoteUrls = new Map()

  eventEmitter.on(AFTER_CLASSIFY_CONTENT, async (playbook, contentCatalog) => {
    const startDir = playbook.dir || '.'
    const { cacheDir, fetch } = playbook.runtime

    await ensureCacheDir(cacheDir, startDir).then((resolvedCacheDir) =>
      Promise.all(contentCatalog.getAll()
        .filter((page) => page.src.family === 'page' || page.src.family === 'partial')
        .reduce((accum, page) => {
          const content = page.contents.toString()

          REMOTE_INCLUDE_RX_TEST.lastIndex = 0
          if (REMOTE_INCLUDE_RX_TEST.test(content)) {
            var match
            REMOTE_INCLUDE_RX.lastIndex = 0
            while ((match = REMOTE_INCLUDE_RX.exec(content))) {
              const scheme = match[1]
              const path = match[2]
              if (path) {
                accum.push(fetchPath(scheme, path, resolvedCacheDir, fetch))
              }
            }
          }
          return accum
        }, [])
      ))
  })

  eventEmitter.on(BEFORE_CONVERT_DOCUMENTS, ({ asciidocConfig }) => {
    asciidocConfig.extensions || (asciidocConfig.extensions = [])
    asciidocConfig.extensions.push(remoteIncludeProcessor)
  })

  async function fetchPath (scheme, path, cacheDir, fetch) {
    await fs.stat(cacheDir).catch(() => false).then((stat) => stat && stat.isDirectory())
    const url = scheme + '://' + path
    const hash = createHash('sha1')
    hash.update(url)
    const cacheKey = hash.digest('hex')
    const cachePath = ospath.join(cacheDir, cacheKey)
    if (!fetch && await fs.stat(cachePath).catch(() => false).then((stat) => stat && stat.isFile())) {
      return readFile(url, cachePath)
    }

    if (!remoteUrls.has(url)) {
      if (scheme === 'http') {
        if (allowHttp) {
          return new Promise((resolve, reject) => {
            http.get(url, (res) => {
              res.setEncoding('utf8')
              var content = ''
              res.on('data', (data) => {
                content += data
              })
              res.on('end', () => {
                remoteUrls.set(url, content)
                writeFile(cachePath, content, resolve)
              })
            }).on('error', (e) => {
              resolve()
            })
          })
        } else {
          console.log('http not allowed: url: ' + url)
        }
      } else if (scheme === 'https') {
        return new Promise((resolve, reject) => {
          https.get(url, { rejectUnauthorized: tlsRejectUnauthorized }, (res) => {
            res.setEncoding('utf8')
            var content = ''
            res.on('data', (data) => {
              content += data
            })
            res.on('end', () => {
              remoteUrls.set(url, content)
              writeFile(cachePath, content, resolve)
            })
          }).on('error', (e) => {
            console.log('https error: ' + e + ' for url: ' + url)
            resolve()
          })
        })
      } else if (scheme === 'file') {
        for (const allowedPath of allowedFilePaths) {
          if (path.startsWith(allowedPath)) {
            return readFile(url, path)
          }
        }
        console.log(`path ${path} not allowed`)
      }
    }
  }

  function readFile (url, path) {
    return fs.readFile(path)
      .then((data) => remoteUrls.set(url, data.toString()))
      .catch((err) => console.log('error reading file: ' + err + ' at path ' + path))
  }

  function writeFile (cachePath, content, resolve) {
    fs.writeFile(cachePath, content, () => resolve())
  }

  //Copied from content-aggregator.
  /**
   * Expands the content cache directory path and ensures it exists.
   *
   * @param {String} preferredCacheDir - The preferred cache directory. If the value is undefined,
   *   the user's cache folder is used.
   * @param {String} startDir - The directory to use in place of a leading '.' segment.
   *
   * @returns {Promise<String>} A promise that resolves to the absolute content cache directory.
   */
  function ensureCacheDir (preferredCacheDir, startDir) {
    // QUESTION should fallback directory be relative to cwd, playbook dir, or tmpdir?
    const baseCacheDir =
      preferredCacheDir == null
        ? getCacheDir('antora' + (process.env.NODE_ENV === 'test' ? '-test' : '')) || ospath.resolve('.antora/cache')
        : expandPath(preferredCacheDir, '~+', startDir)
    const cacheDir = ospath.join(baseCacheDir, CONTENT_CACHE_FOLDER, REMOTE_INCLUDES_FOLDER)
    return fs.ensureDir(cacheDir).then(() => cacheDir)
  }

  //new, more or less
  const remoteIncludeProcessor = {
    register: (registry, { file, contentCatalog }) => {
      registry.includeProcessor(function () {
        this.$option('position', '>>')
        this.handles((target) => target.startsWith('https://') || target.startsWith('http://') || target.startsWith('file:///'))
        this.process((doc, reader, target, attrs) => {
          if (reader.$include_depth() >= Opal.hash_get(reader.maxdepth, 'abs')) {
            if (Opal.hash_get(reader.maxdepth, 'abs')) {
              log('error', `maximum include depth of ${Opal.hash_get(reader.maxdepth, 'rel')} exceeded`, reader)
            }
            return
          }
          const contents = remoteUrls.get(target)
          // if (contents) {
          //   reader.pushInclude(contents, target, target, 1, attrs)
          // } else {
          //   //TODO at least log the problem... throw an exception? mark it somehow?
          //   reader.pushInclude(`Include target ${target} could not be fetched`, target, target, 1, attrs)
          // }

          // const resolvedFile = this[$callback](doc, target, reader.$cursor_at_prev_line())
          if (contents) {
            let includeContents
            let linenums
            let tags
            let startLineNum
            if ((linenums = getLines(attrs))) {
              ;[includeContents, startLineNum] = filterLinesByLineNumbers(reader, target, contents, linenums)
            } else if ((tags = getTags(attrs))) {
              ;[includeContents, startLineNum] = filterLinesByTags(reader, target, contents, tags)
            } else {
              includeContents = contents
              startLineNum = 1
            }
            attrs['partial-option'] = ''
            reader.pushInclude(includeContents, target, target, startLineNum, attrs)
            ;(reader.file = new String(reader.file)).context = target // eslint-disable-line no-new-wrappers
          } else {
            log('error', `include target not found: ${target}`, reader)
            reader.$unshift(`Unresolved include directive in ${reader.$cursor_at_prev_line().file} - include::${target}[]`)
          }
        })
      })
    },
  }

  //Copied from asciidoc-loader include processor
  const DBL_COLON = '::'
  const DBL_SQUARE = '[]'

  const NEWLINE_RX = /\r\n?|\n/
  const TAG_DIRECTIVE_RX = /\b(?:tag|(e)nd)::(\S+?)\[\](?=$|[ \r])/m

  function getLines (attrs) {
    if (attrs.lines) {
      const lines = attrs.lines
      if (lines) {
        const linenums = []
        let filtered
        ;(~lines.indexOf(',') ? lines.split(',') : lines.split(';'))
          .filter((it) => it)
          .forEach((linedef) => {
            filtered = true
            let delim
            let from
            if (~(delim = linedef.indexOf('..'))) {
              from = linedef.substr(0, delim)
              let to = linedef.substr(delim + 2)
              if ((to = parseInt(to) || -1) > 0) {
                if ((from = parseInt(from) || -1) > 0) {
                  linenums.push(...Array.from({ length: to - from + 1 }, (_, i) => i + from))
                }
              } else if (to === -1 && (from = parseInt(from) || -1) > 0) {
                linenums.push(from, Infinity)
              }
            } else if ((from = parseInt(linedef) || -1) > 0) {
              linenums.push(from)
            }
          })
        if (linenums.length) return [...new Set(linenums.sort((a, b) => a - b))]
        if (filtered) return []
      }
    }
  }

  function getTags (attrs) {
    if (attrs.tag) {
      const tag = attrs.tag
      if (tag && tag !== '!') {
        return tag.charAt() === '!' ? new Map().set(tag.substr(1), false) : new Map().set(tag, true)
      }
    } else if (attrs.tags) {
      const tags = attrs.tags
      if (tags) {
        const result = new Map()
        let any = false
        tags.split(~tags.indexOf(',') ? ',' : ';').forEach((tag) => {
          if (tag && tag !== '!') {
            any = true
            tag.charAt() === '!' ? result.set(tag.substr(1), false) : result.set(tag, true)
          }
        })
        if (any) return result
      }
    }
  }

  function filterLinesByLineNumbers (reader, target, contents, linenums) {
    let lineNum = 0
    let startLineNum
    let selectRest
    const lines = []
    contents.split(NEWLINE_RX).some((line) => {
      lineNum++
      if (selectRest || (selectRest = linenums[0] === Infinity)) {
        if (!startLineNum) startLineNum = lineNum
        lines.push(line)
      } else {
        if (linenums[0] === lineNum) {
          if (!startLineNum) startLineNum = lineNum
          linenums.shift()
          lines.push(line)
        }
        if (!linenums.length) return true
      }
    })
    return [lines, startLineNum || 1]
  }

  function filterLinesByTags (reader, target, contents, tags) {
    let selecting, selectingDefault, wildcard
    if (tags.has('**')) {
      if (tags.has('*')) {
        selectingDefault = selecting = tags.get('**')
        wildcard = tags.get('*')
        tags.delete('*')
      } else {
        selectingDefault = selecting = wildcard = tags.get('**')
      }
      tags.delete('**')
    } else {
      selectingDefault = selecting = !Array.from(tags.values()).includes(true)
      if (tags.has('*')) {
        wildcard = tags.get('*')
        tags.delete('*')
      }
    }

    const lines = []
    const tagStack = []
    const foundTags = []
    let activeTag
    let lineNum = 0
    let startLineNum
    contents.split(NEWLINE_RX).forEach((line) => {
      lineNum++
      let m
      if (~line.indexOf(DBL_COLON) && ~line.indexOf(DBL_SQUARE) && (m = line.match(TAG_DIRECTIVE_RX))) {
        const thisTag = m[2]
        if (m[1]) {
          if (thisTag === activeTag) {
            tagStack.shift()
            ;[activeTag, selecting] = tagStack.length ? tagStack[0] : [undefined, selectingDefault]
          } else if (tags.has(thisTag)) {
            const idx = tagStack.findIndex(([name]) => name === thisTag)
            if (~idx) {
              tagStack.splice(idx, 1)
              log(
                'warn',
                `mismatched end tag (expected '${activeTag}' but found '${thisTag}') ` +
                `at line ${lineNum} of include file: ${target})`,
                reader,
                reader.$create_include_cursor(target, target, lineNum)
              )
            } else {
              log(
                'warn',
                `unexpected end tag '${thisTag}' at line ${lineNum} of include file: ${target}`,
                reader,
                reader.$create_include_cursor(target, target, lineNum)
              )
            }
          }
        } else if (tags.has(thisTag)) {
          foundTags.push(thisTag)
          tagStack.unshift([(activeTag = thisTag), (selecting = tags.get(thisTag)), lineNum])
        } else if (wildcard !== undefined) {
          selecting = activeTag && !selecting ? false : wildcard
          tagStack.unshift([(activeTag = thisTag), selecting, lineNum])
        }
      } else if (selecting) {
        if (!startLineNum) startLineNum = lineNum
        lines.push(line)
      }
    })
    if (tagStack.length) {
      tagStack.forEach(([tagName, _, tagLineNum]) =>
        log(
          'warn',
          `detected unclosed tag '${tagName}' starting at line ${tagLineNum} of include file: ${target}`,
          reader,
          reader.$create_include_cursor(target, target, tagLineNum)
        )
      )
    }
    if (foundTags.length) foundTags.forEach((name) => tags.delete(name))
    if (tags.size) {
      const missingTagNames = Array.from(tags.keys())
      log(
        'warn',
        `tag${tags.size > 1 ? 's' : ''} '${missingTagNames.join(', ')}' not found in include file: ${target}`,
        reader
      )
    }
    return [lines, startLineNum || 1]
  }

  function log (severity, message, reader, includeCursor = undefined) {
    const opts = includeCursor
      ? { source_location: reader.$cursor_at_prev_line(), include_location: includeCursor }
      : { source_location: reader.$cursor_at_prev_line() }
    reader.$logger()['$' + severity](reader.$message_with_context(message, Opal.hash(opts)))
  }
}
