/* eslint-env mocha */
'use strict'

process.env.NODE_ENV = 'test'

const EventEmitter = require('events')
const chai = require('chai')
const { expect } = require('chai')
chai.use(require('chai-fs'))
chai.use(require('chai-cheerio'))
chai.use(require('chai-spies'))
// dirty-chai must be loaded after the other plugins
// see https://github.com/prodatakey/dirty-chai#plugin-assertions
chai.use(require('dirty-chai'))
const fs = require('fs')
const fsextra = require('fs-extra')
const http = require('http')
const https = require('https')
const loadAsciidoc = require('@antora/asciidoc-loader')
// QUESTION should the cache version track the major version of Antora?
// const CONTENT_CACHE_FOLDER = 'content/2'
const getCacheDir = require('cache-directory')

const remoteIncludeExtension = require('../lib/index')
//events
const AFTER_CLASSIFY_CONTENT = 'afterClassifyContent'
const BEFORE_CONVERT_DOCUMENTS = 'beforeConvertDocuments'

const CACHE_DIR = getCacheDir('antora-test')

function clean () {
  removeSyncForce(CACHE_DIR)
}

function removeSyncForce (p, timeout = 5000) {
  // NOTE remove can fail multiple times on Windows, so try, try again
  if (process.platform === 'win32') {
    const start = Date.now()
    let retry = true
    while (retry) {
      try {
        fsextra.removeSync(p)
        retry = false
      } catch (err) {
        if (Date.now() - start > timeout) throw err
      }
    }
  } else {
    fsextra.removeSync(p)
  }
}

describe('https remote-include tests', () => {
  var httpServer
  var httpsServer
  var eventEmitter
  var playbookSpec
  var httpRequests

  const site = {
    'patha/to/a.adoc': {
      mediaType: 'text/asciidoc',
      contents: `= A include

With a bit of content.
`,
      title: 'A include',
      body: 'With a bit of content.',
    },
    'pathb/to/b.adoc': {
      mediaType: 'text/asciidoc',
      contents: `= B include
      
With alternate content
`,
    },
    'examples/greet.rb': {
      mediaType: 'text/ruby',
      contents: `# hello
puts "Hello, World!"
# goodbye
puts "Goodbye, World!"`,
    },
    'examples/greet-tag.rb': {
      mediaType: 'text/ruby',
      contents: `# greet example
# tag::hello[]
puts "Hello, World!"
# end::hello[]`,
    },
  }

  function contentCatalog (content) {
    return {
      getAll: () => {
        return [{
          src: {
            family: 'page',
            relative: 'page.adoc',
          },
          pub: {
            moduleRootPath: '../..',
          },
          contents: Buffer.from(content),
        },
        {
          src: {
            family: 'alias',
            relative: 'alias.adoc',
          },
          pub: {
            moduleRootPath: '../..',
          },
        }]
      },
    }
  }

  before(async () => {
    const options = {
      key: fs.readFileSync('test/fixtures/keys/server.key'),
      cert: fs.readFileSync('test/fixtures/keys/server.cert'),
    }

    httpServer = http.createServer(options, (req, resp) => {
      const url = req.url.slice(1)
      const file = site[url]
      if (file) {
        httpRequests.push(file)
        const type = file.mediaType
        resp.writeHead(200, `{content-type: ${type}}`)
        resp.write(file.contents)
        resp.end()
      } else {
        resp.writeHead(404, 'file not found')
        resp.end()
      }
    }).listen({ port: 8081, hostname: 'localhost' })
    httpsServer = https.createServer(options, (req, resp) => {
      const url = req.url.slice(1)
      const file = site[url]
      if (file) {
        httpRequests.push(file)
        const type = file.mediaType
        resp.writeHead(200, `{content-type: ${type}}`)
        resp.write(file.contents)
        resp.end()
      } else {
        resp.writeHead(404, 'file not found')
        resp.end()
      }
    }).listen({ port: 8082, hostname: 'localhost' })
  })

  after(async () => {
    httpServer.close()
    httpsServer.close()
  })

  beforeEach(() => {
    playbookSpec = {
      runtime: { quiet: true },
    }
    clean()
    httpRequests = []
  })

  afterEach(() => {
    clean()
  })

  function setup (config = {
    allowHttp: true,
    TESTINGRejectUnauthorized: 'test.only.false',
    allowedFilePaths: [`${__dirname}`],
  }) {
    const baseEmitter = new EventEmitter()

    eventEmitter = {

      emit: async (name, ...args) => {
        const promises = []
        baseEmitter.emit(name, promises, ...args)
        promises.length && await Promise.all(promises)
      },

      on: (name, listener) => baseEmitter.on(name, (promises, ...args) => promises.push(listener(...args))),
    }
    remoteIncludeExtension.register(eventEmitter, config)
  }

  ;[
    'http://localhost:8081',
    'https://localhost:8082',
    `file://${__dirname}/fixtures`,
  ].forEach((protocol) => {
    it(`should fetch ${protocol.slice(0, 5)} include`, async () => {
      setup()
      const cc = contentCatalog(`= The Inclusive page

include::${protocol}/patha/to/a.adoc[leveloffset=+1]   
  include::${protocol}/pathb/to/b.adoc[leveloffset=+1]
`)

      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)

      const asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      const doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig).convert()
      expect(doc).to.include(site['patha/to/a.adoc'].title)
      expect(doc).to.include(site['patha/to/a.adoc'].body)
      expect(doc).to.include(`include::${protocol}/pathb/to/b.adoc[leveloffset=+1]`)
    })

    it(`should not fetch ${protocol.slice(0, 5)} include`, async () => {
      setup({})
      const path = `${protocol}/patha/to/a.adoc`
      const cc = contentCatalog(`= The Inclusive page

include::${path}[leveloffset=+1]   
  include::${protocol}/pathb/to/b.adoc[leveloffset=+1]
`)

      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)

      const asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      const doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig).convert()
      expect(doc).to.include(`Unresolved include directive in  - include::${path}[]`)
      expect(doc).to.include(`include::${protocol}/pathb/to/b.adoc[leveloffset=+1]`)
    })

    it(`should apply linenum filtering to contents of include if lines separated by commas are specified with protocol ${protocol.slice(0, 5)}`, async () => {
      setup()
      const cc = contentCatalog(`
[source,ruby]
----
include::${protocol}/examples/greet.rb[lines="2,4"]
----
`)

      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)

      const asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      const doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig)
      const firstBlock = doc.getBlocks()[0]
      expect(firstBlock).not.to.be.undefined()
      expect(firstBlock.getContext()).to.equal('listing')
      expect(firstBlock.getSourceLines()).to.eql(site['examples/greet.rb'].contents.split('\n').filter((l) => l.charAt() !== '#'))
    })

    it(`should apply tag filtering to contents of include if tag is specified with protocol ${protocol.slice(0, 5)}`, async () => {
      setup()
      const cc = contentCatalog(`
[source,ruby]
----
include::${protocol}/examples/greet-tag.rb[tag=hello]
----
`)

      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)

      const asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      const doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig)
      const firstBlock = doc.getBlocks()[0]
      expect(firstBlock).not.to.be.undefined()
      expect(firstBlock.getContext()).to.equal('listing')
      expect(firstBlock.getSourceLines()).to.eql(site['examples/greet-tag.rb'].contents.split('\n').filter((l) => l.charAt() !== '#'))
    })
  })

  ;[
    'http://localhost:8081',
    'https://localhost:8082',
  ].forEach((protocol) =>
    it(`should fetch ${protocol.slice(0, 5)} include from cache`, async () => {
      setup()
      const cc = contentCatalog(`= The Inclusive page

include::${protocol}/patha/to/a.adoc[leveloffset=+1]   
  include::${protocol}/pathb/to/b.adoc[leveloffset=+1]
`)

      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)
      expect(httpRequests.length).to.equal(1)

      var asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      var doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig).convert()
      expect(doc).to.include(site['patha/to/a.adoc'].title)
      expect(doc).to.include(site['patha/to/a.adoc'].body)
      expect(doc).to.include(`include::${protocol}/pathb/to/b.adoc[leveloffset=+1]`)

      setup()
      await eventEmitter.emit(AFTER_CLASSIFY_CONTENT, playbookSpec, cc)
      expect(httpRequests.length).to.equal(1)

      asciidocConfig = {}
      await eventEmitter.emit(BEFORE_CONVERT_DOCUMENTS, { asciidocConfig })
      expect(asciidocConfig.extensions).to.not.equal(null)
      expect(asciidocConfig.extensions.length).to.equal(1)
      doc = loadAsciidoc(cc.getAll()[0], null, asciidocConfig).convert()
      expect(doc).to.include(site['patha/to/a.adoc'].title)
      expect(doc).to.include(site['patha/to/a.adoc'].body)
      expect(doc).to.include(`include::${protocol}/pathb/to/b.adoc[leveloffset=+1]`)
    }))
})
